# X-Wing Squadbuilder #

Dieses Projekt setzt auf einen neuen/alten Ansatz zum Squadbuilding für das X-Wing Miniaturenspiel: Das Kartenschubsen.

![squadbuilder.png](https://bitbucket.org/repo/ybLdnb/images/3970162818-squadbuilder.png)

## Karten auf den Tisch ##

Statt über Dropdowns und Buttons Listen zu erstellen folgt es dem intuitiven Ansatz Karten auf einem Desktop hinzuzufügen und hin und her schieben zu können, als hätte man Karten auf dem Tisch.
Durch das schieben kann man leicht optisch Gruppen von Upgrades und Pilotenkarten bilden und wieder auflösen. Auch einzelne Geschwader können so gebildet werden.

## Punktezähler ##
Wird eine Karte aus dem Menü auf den Desktop hinzugefügt, steigt der Punktezähler. Wird sie entfernt, sinkt er. Der Zähler hat dabei kein Limit, so das man ohne Einstellungen mit mehr Punkten auf dem Tisch arbeiten kann.

## Persönliche Sammlung ##

Alle Karten werden aus einem Menü auf den Desktop hinzugefügt. Dieses Menü wird dynamisch aus der Ordnerstruktur des "data"-Ordners ausgelesen. Dies ermöglicht es seine persönliche Sammlung anzugeben. Dabei ist es möglich jeder Karte eine Anzahl mit zu geben, wie häufig sie in der Sammlung vertreten ist. Diese Anzahl wird im Menü eingeblendet und genutzt, damit auf dem Desktop nicht mehr Karten dieses Typs landen können, als man mit seiner Sammlung aufstellen kann.

### Kartensammlung anlegen ###

Jeder Unterordner einer Fraktion legt ein Schiffsmenü mit diesem Namen an. Jedes Grafik darunter wird als Pilotenkarte angelegt. Das gleiche gilt für Upgrades, Upgrademenüs und -karten.

* data
    * empire
        * TIE-Fighter
            * Pilot1.jpg
            * Pilot2.jpg
            * Pilot3.jpg
            * Pilot4.jpg
        * TIE-Bomber
            * ...
    * rebel
        * X-Wing
            * ...
    * scum
        * ...
    * upgrade
        * Astromech
            * ...

#### Benennung der Grafiken ####

Jede Grafik muss nach einem bestimmten Namensschema benannt sein. Der Anfang des Dateinamens einer Grafik gibt

* die Punktekosten der Karte
* die Anzahl der Verfügbaren Karten in der Sammlung

an. Das Schema sieht wie folgt aus: *[KOSTEN]-[ANZAHL]-[RESTLICHER DATEINAME]*

Ein Beispiel: Eine Grafik mit dem Namen "XWing-Wedge.jpg" soll mit 29 Punkten berechnet werden und ist ein Mal in der Sammlung vorhanden. Der Dateiname lautet nach dem Schema "29-1-XWing-Wedge.jpg".

Negative Punktekosten werden mit einer voranstehenden Tilde (~) statt einem Minus angegeben. Die Chardaan Nachrüstung beispielsweise bekäme wenn sie zweimal in der Sammlung auftauchen soll den Dateinamen "~2-2-Chardaan".

Variable Punkteerstattungen, wie bei dem Titel TIE/x1, können bisher nicht angegeben werden. Hier kann man sich behelfen, indem man einfach eine höhere Punkteanzahl akzeptiert, deren Wert den Upgrades entspricht, die der Karte zugeordnet sind. Keine perfekte Lösung aber auch nicht untragbar.

#### Grafiken der Karten ####

Dieses Projekt stellt keine Grafiken der Karten zur Verfügung, sie müssen selbst hinzugefügt werden.

## Mobile Nutzung ##

Diese Web-App lässt sich sehr gut auch auf Tablets nutzen und ermöglicht es damit schnell mal zwischendurch ein neues Geschwader auf die Beine zu stellen. Ob von der Couch aus oder zwischen zwei Matches. 

## Squads speichern ##

Die Web-App verfügt derzeit nicht über eine Speicheroption. Aber mit einem Screenshot ist die Zusammenstellung schnell gesichert und der Shot kann für spätere Referenz in die Cloud geladen werden, von wo man z.B. vom Smartphoneauf ihn zugreifen kann.