<?php
	$daten = json_decode(rawurldecode(file_get_contents("php://input")), true);
	
	$aufruf = $daten["aufruf"];
	
	//SUBKATEGORIEN
	if($aufruf == "subkategorien"){
		$kategorie = $daten["kategorie"];
		
		$ordner = "../data/".$kategorie."/";
		
		if(is_dir($ordner)) {
			$rueckgabe = array();
			$eintraege = scandir($ordner);
			
			foreach($eintraege as $eintrag){
				if (!is_dir($eintrag)) {
					$unterOrdner = $ordner . $eintrag;
					if(is_dir($unterOrdner)) {
						$unterEintraege = scandir($unterOrdner);
						$rueckgabe[$eintrag] = $unterEintraege[2];
					}
				}
			}
	  	}
		
		echo json_encode($rueckgabe);
	}
	
	//KATEGORIEN
	else if($aufruf == "eintraege"){
		$kategorie = $daten["kategorie"];
		$subkategorie = $daten["subkategorie"];
		
		$ordner = "../data/".$kategorie."/".$subkategorie."/";
		
		if(is_dir($ordner)) {
			$rueckgabe = array();
			$eintraege = scandir($ordner);
			
			foreach($eintraege as $eintrag){
				if (!is_dir($eintrag)) {
					array_push($rueckgabe, $eintrag);
				}
			}
	  	}
		
		echo json_encode($rueckgabe);
	}
?>