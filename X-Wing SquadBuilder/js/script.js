var Menu = function(selector, pointsArea){
	
	var that = this;
	
	var button = $("#" + selector + "Button");
	var menu = $("#" + selector);
	
	constructor();
	
	function constructor(){
		button.click(function(e){
			
			closeMenu();
			
			if(!menu.hasClass("open")){
				getSubcategories();
			}
			
			button.toggleClass("open");
			menu.toggleClass("open");
		});
	}
	
	function closeMenu(){
		$("#addMenu img:not(#"+selector+"Button)").removeClass("open");
		$(".menu:not(#"+selector+")").removeClass("open");
		$(".subCategoryButton").remove();
		
		closeSubmenu();
	}
	
	function closeAllMenu(){
		closeMenu();
		$(".menu#"+selector).removeClass("open");
		button.removeClass("open");
	}
	
	function getSubcategories(){
		var data = {};
		data.aufruf = "subkategorien";
		data.kategorie = selector;
		
		data = encodeURIComponent(JSON.stringify(data));
		
		$.ajax({
			  url: "backend/GetEntries.php",
			  type: "post",
			  dataType: "json",
			  data: data
			})
			.done(function(msg){
				showSubcategories(msg);
			})
			.fail(function(){
				console.log("ERR: Could not retrieve subcategories for " + selector);
		});
	}
	
	function showSubcategories(subcategories){
		console.log(subcategories);
		
		for(var subcategory in subcategories) {
			var src = "data/"+selector+"/"+subcategory+"/"+subcategories[subcategory];
			var entry = $("<table id=\""+subcategory+"\" class=\"subCategoryButton\"><tr><td style=\"background-image: url("+src+")\"></td></tr><tr><td>"+subcategory+"</td></tr></table>");
			entry.click(function(e){
				var submenu = $("<div class=\"submenu\"></div>"); 
				var closeButton = $("<div class=\"closeButton\"><i class=\"fa fa-times\"></i></div>");
				submenu.append(closeButton);
				closeButton.click(function(){ closeSubmenu(); });
				menu.append(submenu);
				
				getEntries($(e.currentTarget).attr("id"));
			});
			menu.append(entry);
		}
	}
	
	function closeSubmenu(){
		$("div.closeButton").remove();
		$("div.submenu").remove();
	}

	function getEntries(subcategory){
		var data = {};
		data.aufruf = "eintraege";
		data.kategorie = selector;
		data.subkategorie = subcategory;
		
		data = encodeURIComponent(JSON.stringify(data));
		
		$.ajax({
			  url: "backend/GetEntries.php",
			  type: "post",
			  dataType: "json",
			  data: data
			})
			.done(function(msg){
				showEntries(msg, subcategory);
			})
			.fail(function(){
				console.log("ERR: Could not retrieve entries for " + selector + "/" + subcategory);
		});
	}
	
	function showEntries(entries, subcategory){
		for (index = 0; index < entries.length; ++index) {
			var src = "data/"+selector+"/"+subcategory+"/"+entries[index];
			var entryParts = entries[index].split("-");
			var points = entryParts[0].replace("#", "-");
			
			
			var number = entryParts[1];
			
			var numberInSquad = number - getNumberInSquad(src);
			var entry = $("<div class=\"entry\"><img src=\""+src+"\" points=\""+points+"\" \"><div class=\"number\">"+numberInSquad+"</div></div>");
			
			if(numberInSquad <= 0){
				entry.addClass("disabled");
				entry.click(function(e){
					closeSubmenu();
				});
			}
			else{
				entry.click(function(e){
					closeSubmenu();
					
					var absPath = $(e.currentTarget).children("img")[0].src;
					var relPath = "data" + absPath.split("data")[1];
					
					var entryParts = absPath.split("/");
					var entrySrc = entryParts[entryParts.length-1].split("-");
					var points = entrySrc[0].replace("~", "-");;
					
					var card = $("<img class='"+selector+"' src='"+relPath+"' points=\""+points+"\" />");
					card.draggable({ snap: true });
					$("#squad").append(card);
					pointsArea.update();
				});
			}
			
			$(".submenu").append(entry);
		}
	}
	
	function getNumberInSquad(src){
		return $("#squad img[src=\""+src+"\"]").length;
	}
	
};

var DeletionArea = function(pointsArea){
	
	var that = this;
	
	constructor();
	
	function constructor(){
		$( "#addMenu" ).droppable({
			over: function( event, ui ) {
	        	$(ui.draggable).addClass("removeIndicator");
	      	},
	      	out: function( event, ui ) {
	        	$(ui.draggable).removeClass("removeIndicator");
	      	},
	      	drop: function( event, ui ) {
	        	$(ui.draggable).remove();
	        	pointsArea.update();
	      	}
	    });
	}
};

var PointsArea = function(){
	
	var that = this;
	
	constructor();
	
	function constructor(){
	}
	
	this.update = function(){
			var cards = $("#squad").children("img");
			$( "#pointsArea span" ).text(0);
			
			cards.each(function(){
				var pointString = $(this).first().attr("points");
				
				var points = parseInt(pointString);
				
				var pointCurrentString = $( "#pointsArea span" ).text();
				var pointCurrent = parseInt(pointCurrentString);
				
				$( "#pointsArea span" ).text(points+pointCurrent);
			});
	};
};